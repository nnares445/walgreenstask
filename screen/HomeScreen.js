import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

const HomeScreen = ({ navigation }) => {
    return (
        <View style={styles.homeScreenContainer}>
            <Text>Home Screen</Text>
            <Button
                color="#ff0000"
                title="Goto Today's Task"
                onPress={() => navigation.navigate('Task')}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    homeScreenContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#f0ffff',
        padding: 16
    }
});

export default HomeScreen;