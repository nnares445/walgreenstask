import 'react-native-gesture-handler';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { StyleSheet, Text, View } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from './screen/HomeScreen';
import TaskScreen from './screen/TaskScreen';


const Stack = createStackNavigator();

export default function App() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Home">
                <Stack.Screen name="Home"
                    component={HomeScreen}
                    options={{
                        title: 'Home Screen',
                        headerStyle: {
                            backgroundColor: '#87cefa',
                        },
                        headerTitileStyle: {
                            fontWeight: 'bold',
                        },
                        headerTintColor: '#404040',
                        headerBackTitle: ' '
                    }}
                />
                <Stack.Screen name="Task" component={TaskScreen}
                    options={{
                        title: 'Contribute Walgreens Cash',
                        headerStyle: {
                            backgroundColor:'#87cefa',
                        },
                        headerTitileStyle: {
                            fontWeight: 'bold',
                        },
                        headerTintColor:'#404040',
                        headerBackTitle: ' '
                    }}
                />
            </Stack.Navigator>
        </NavigationContainer>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
