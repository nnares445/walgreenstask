import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';


const ContainerListRow = (props) => {
    return (
        <View style={styles.localCausesListContainer}>
            <View style={styles.walgreensLogoContainer}>
                <Image
                    style={styles.walgreensLogo}
                    source={require('../assets/walgreens_logo2.png')} />
            </View>
            <View style={styles.localCausesListMessageContainer}>
                <Text style={styles.otherSubHeaderTitle}>{props.header} </Text>
                <Text style={styles.monthlyGoalText}>{props.subHeader}</Text>
                <Text style={styles.toContributeMessageText}>{props.message} </Text>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    localCausesListContainer: {
        flexDirection: 'row'
    },

    localCausesListMessageContainer: {
         paddingRight:30,
         marginRight: 10
    },

    walgreensLogoContainer: {
        alignSelf: 'flex-start',
        borderRadius: 50,
        marginRight: 10,
        borderWidth: 0,
        shadowColor: 'black',
        shadowOffset: { width: 1, height: 4 },
        shadowRadius: 3,
        shadowOpacity: 0.4,
        elevation: 1,
        ...Platform.select({
            ios: {
                borderWidth: 1,
            }
        })

    },

    walgreensLogo: {
        width: 70,
        height: 70
    },

    otherSubHeaderTitle: {
        fontSize: 22,
        color: '#404040',
        fontWeight: 'bold',
        flexWrap: 'wrap',
        marginTop: 5,
       paddingRight: 10,
       marginRight:10
    },

    monthlyGoalText: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#404040',
        paddingRight:20,
        marginRight:10
    },

    toContributeMessageText: {
        fontSize: 17,
        color: '#404040',
        paddingRight: 30,
        marginRight: 10
    },
});

export default ContainerListRow;