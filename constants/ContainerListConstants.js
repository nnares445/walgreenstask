
export let props = {
    header: "PAWS Chicago ",
    subHeader: "Why would your money do?",
    message: "Help out the cats and dogs by donating your rewards for enrichment toys!"
}

export let propsII = {
    header: "Center on Halsted ",
    subHeader: "Why would your money do?",
    message: "Help to keep fighting for no discrimination among LGBTQIA+ in the workplace."
}

export let propsIII = {
    header: "Greater Chicago Food...",
    subHeader: "Why would your money do?",
    message: "Width COVID-19 pandemic, many families are without food, help us provide meals for them"
}

export let propsIV = {
    header: "Human Rights Campaign",
    subHeader: "Why would your money do?",
    message: "Basic rights are being stripped down with the current presidency for LGBTQA+ community."
}